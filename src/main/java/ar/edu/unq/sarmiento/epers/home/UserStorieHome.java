package ar.edu.unq.sarmiento.epers.home;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.unq.sarmiento.epers.model.Backlog;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Repository
public class UserStorieHome extends AbstractHome<UserStorie>{

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public UserStorie findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Iteracion> findIteraciones(int proyectoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserStorie> findUserStoriesPorHacer(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void delete(UserStorie userStorie){
		this.getSession().delete(userStorie);
	}
	
	
	
	public List<UserStorie> getUserStoriesSinIteracionParaUn(Backlog backlog) {
		Criteria c = this.getSession().createCriteria(UserStorie.class);
		c.add(Restrictions.eq("backlog", backlog));
		c.add(Restrictions.isNull("iteracion"));
		List result = c.list();
		return result;
	}
}
