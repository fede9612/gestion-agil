package ar.edu.unq.sarmiento.epers.home;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.model.Usuario;
import ar.edu.unq.sarmiento.epers.model.UsuarioProyecto;

@Component
public class UsuarioProyectoHome extends AbstractHome<UsuarioProyecto>{
	
	@Override
	public UsuarioProyecto findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Iteracion> findIteraciones(int proyectoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserStorie> findUserStoriesPorHacer(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<UsuarioProyecto> findUsuariosProyecto(int idProyecto) {
		return this.getSession()
				.createQuery("FROM UsuarioProyecto WHERE proyecto_id = :id")
				.setParameter("id", idProyecto)
				.getResultList();
	}

}
