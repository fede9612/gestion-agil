package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.Estado;
import ar.edu.unq.sarmiento.epers.wicket.iteracion.DetalleIteracionPage;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.ConfiguracionProyectoPage;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.ConfigurarProyectoController;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.HomeProyecto;

public class ConfigurarUserStoriePage extends WebPage{

	@SpringBean(name="configurarUserStorieController")
	private ConfigurarUserStorieController controller;
	
	public ConfigurarUserStoriePage(int userStorieId, int proyectoId, int iteracionId) {
		super();
		controller.setUserStorieId(userStorieId);
		controller.setProyectoId(proyectoId);
		
		this.add(new Label("nombre", new PropertyModel<>(controller, "nombre")));
		this.add(new Label("estado", new PropertyModel<>(controller, "estado")));
		this.add(new Link<String>("estadoPorHacerBtn") {

			@Override
			public void onClick() {
				controller.cambiarEstado(Estado.PorHacer);
				this.setResponsePage(new ConfigurarUserStoriePage(userStorieId, proyectoId, iteracionId));
			}
			
		});
		this.add(new Link<String>("estadoHaciendoBtn") {

			@Override
			public void onClick() {
				controller.cambiarEstado(Estado.Haciendo);
				this.setResponsePage(new ConfigurarUserStoriePage(userStorieId, proyectoId, iteracionId));
			}
			
		});
		this.add(new Link<String>("estadoHechoBtn") {

			@Override
			public void onClick() {
				controller.cambiarEstado(Estado.Hecho);
				this.setResponsePage(new ConfigurarUserStoriePage(userStorieId, proyectoId, iteracionId));
			}
			
		});
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new DetalleIteracionPage(proyectoId, iteracionId));
			}
			
		});
		
		Form<ConfigurarProyectoController> formUsuario = new Form<ConfigurarProyectoController>("formUsuario"){
			
		};
		
		Button agregarUsuarioBtn = new Button("usuarioBtn"){
            public void onSubmit() {
            	super.onSubmit();
				controller.persistirUsuario();
				this.setResponsePage(new ConfigurarUserStoriePage(userStorieId, proyectoId, iteracionId));
            }
        };
		Button estimarBtn = new Button("estimacionBtn"){
            public void onSubmit() {
                info("Cancel was pressed!");
                controller.persistirEstimacion();
                this.setResponsePage(new ConfigurarUserStoriePage(userStorieId, proyectoId, iteracionId));
            }
        };
		
		DropDownChoice<String> dropChoiceUsuarios = new DropDownChoice<>(
				// id
				"usuariosEnProyecto",
				// binding del valor
				new PropertyModel<>(controller, "usuario"),
				// binding de la lista de items
				new PropertyModel<>(controller, "usuariosParaUnProyecto"),
				// que se muestra de cada item
				new ChoiceRenderer<>("nombre"));
		formUsuario.add(dropChoiceUsuarios);
		formUsuario.add(agregarUsuarioBtn);
		formUsuario.add(estimarBtn);
		formUsuario.add(new Label("estimacionActual", new PropertyModel<>(controller, "estimacionActual")));
		formUsuario.add(new TextField<>("estimacion", new PropertyModel<>(controller, "estimacion")));
		this.add(new Label("nombreUsuario", new PropertyModel<>(controller, "nombreUsuario")));
		
		this.add(formUsuario);
	}
	
}
