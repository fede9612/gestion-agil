package ar.edu.unq.sarmiento.epers.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import org.hibernate.engine.profile.Fetch;

@Entity
@NamedQueries({
	@NamedQuery(name="storiesIteracion", 
			query="FROM UserStorie WHERE iteracion_id = :idIteracion "
						+ "AND estado = :estado"),
})
public class UserStorie extends Persistible{

	@ManyToOne
	private Backlog backlog;
	private String nombre;
	@Column(nullable=true)
	private String descripcion;
	private int estimacion;
	@OneToOne()
	private Iteracion iteracion;
	@Enumerated(EnumType.STRING)
	private Estado estado;
	@OneToOne(fetch = FetchType.LAZY)
	private Usuario usuario;
	
	
	public Backlog getBacklog() {
		return backlog;
	}

	public void setBacklog(Backlog backlog) {
		this.backlog = backlog;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Iteracion getIteracion() {
		return iteracion;
	}

	public void setIteracion(Iteracion iteracion) {
		this.iteracion = iteracion;
		if(!(iteracion == null)){
			iteracion.addUserStorie(this);
	
		}
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public int getEstimacion() {
		return estimacion;
	}

	public void setEstimacion(int estimacion) {
		this.estimacion = estimacion;
	}
	
	public String getNombreIteracion(){
		if(!(this.getIteracion() == null)){
			return this.getIteracion().getNombre();
		}else{
			return "";
		}
	}
	
}
