package ar.edu.unq.sarmiento.epers.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Iteracion extends Persistible{

	@ManyToOne
	private Proyecto proyecto;
	private String nombre;
	@OneToMany
	@JoinColumn(name="iteracion_id")
	private List<UserStorie> userStories = new ArrayList<>();
	@Column
	private boolean abierto;
	@ElementCollection
	@CollectionTable(name="UserStoryPorHacer", joinColumns=@JoinColumn(name="iteracion_id"))
	@Column(name="user_story")
	private List<Integer> userStoriesPorHacer = new ArrayList<>();
	@ElementCollection
	@CollectionTable(name="UserStoryHaciendo", joinColumns=@JoinColumn(name="iteracion_id"))
	@Column(name="user_story")
	private List<Integer> userStoriesHaciendo = new ArrayList<>();
	@ElementCollection
	@CollectionTable(name="UserStoryHecho", joinColumns=@JoinColumn(name="iteracion_id"))
	@Column(name="user_story")
	private List<Integer> userStoriesHecho = new ArrayList<>();
	
	
	public Iteracion() {
	}
	
	public Iteracion(String nombre) {
		this.nombre = nombre;
	}
	
	public Proyecto getProyecto() {
		return proyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void addUserStorie(UserStorie userStorie) {
		this.userStories.add(userStorie);
	}

	public List<UserStorie> getUserStories() {
		return userStories;
	}

	public boolean getAbierto() {
		return abierto;
	}

	public void setAbierto(boolean tof) {
		this.abierto = tof;
	}

	public List<Integer> getUserStoriesPorHacer() {
		return userStoriesPorHacer;
	}

	public void setUserStoriesPorHacer(List<Integer> userStoriesPorHacer) {
		this.userStoriesPorHacer = new ArrayList<>(userStoriesPorHacer);
	}

	public List<Integer> getUserStoriesHaciendo() {
		return userStoriesHaciendo;
	}

	public void setUserStoriesHaciendo(List<Integer> userStoriesHaciendo) {
		this.userStoriesHaciendo = new ArrayList<>(userStoriesHaciendo);
	}

	public List<Integer> getUserStoriesHecho() {
		return userStoriesHecho;
	}

	public void setUserStoriesHecho(List<Integer> userStoriesHecho) {
		this.userStoriesHecho = new ArrayList<>(userStoriesHecho);
	}

	
}
