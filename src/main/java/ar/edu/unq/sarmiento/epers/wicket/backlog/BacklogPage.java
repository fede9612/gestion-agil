package ar.edu.unq.sarmiento.epers.wicket.backlog;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.BotonConfirmar;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.wicket.iteracion.CrearIteracionPage;
import ar.edu.unq.sarmiento.epers.wicket.iteracion.DetalleIteracionPage;
import ar.edu.unq.sarmiento.epers.wicket.iteracion.HistorialIteracionPage;
import ar.edu.unq.sarmiento.epers.wicket.iteracion.ModificarIteracionPage;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.ConfiguracionProyectoPage;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.HomeProyecto;
import ar.edu.unq.sarmiento.epers.wicket.userstorie.CrearUserStoriePage;
import ar.edu.unq.sarmiento.epers.wicket.userstorie.DetalleUserStoriePage;
import ar.edu.unq.sarmiento.epers.wicket.userstorie.ModificarUserStoriePage;

public class BacklogPage extends WebPage{
	
	@SpringBean(name="controllerBacklogPage")
	private ControllerBacklogPage controller;

	public BacklogPage(int idProyecto) {
		super();
		controller.setIdProyecto(idProyecto);
		this.add(new Link<String>("configuracion") {
			
			@Override
			public void onClick() {
				this.setResponsePage(new ConfiguracionProyectoPage(controller.getIdProyecto()));
			}
			
		});
		this.add(new Link<String>("homePage") {
			
			@Override
			public void onClick() {
				this.setResponsePage(new HomeProyecto());
			}
			
		});
		this.add(new Label("nombreProyectoMenu", new PropertyModel<>(this.controller, "proyecto.nombre")));
		this.add(new ListView<Iteracion>("filaIteracion", new PropertyModel<>(this.controller, "iteraciones")) {

			@Override
			protected void populateItem(ListItem<Iteracion> panel) {
				Iteracion iteracion = panel.getModelObject();
				CompoundPropertyModel<Iteracion> iteracionModel = new CompoundPropertyModel<>(iteracion);
				Label nombreIteracion = new Label("nombre", iteracionModel.bind("nombre"));
				panel.add(nombreIteracion);
				Label lblEstimacion = new Label("estimacion", controller.getSumaEstimacion(iteracion));
				Label lblEStimacionPorHacer = new Label("estimacionPorHacer", controller.getSumaEstimacionPorHacer(iteracion));
				Label lblEstimacionHaciendo = new Label("estimacionHaciendo", controller.getSumaEstimacionHaciendo(iteracion));
				Label lblEstimacionHecho = new Label("estimacionHecho", controller.getSumaEstimacionHecho(iteracion));
				panel.add(lblEstimacion);
				panel.add(lblEStimacionPorHacer);
				panel.add(lblEstimacionHaciendo);
				panel.add(lblEstimacionHecho);
				
				
				panel.add(new Label("estado", controller.getEstadoIteracion(iteracion)));
				Link iteracionBtn = new Link<String>("iteracionBtn") {
					private static final long serialVersionUID = 8397905807894993988L;

					@Override
					public void onClick() {
						this.setResponsePage(new DetalleIteracionPage(idProyecto, iteracion.getId()));
					}
					
				};
				
				Link iteracionHistoriaBtn = new Link<String>("iteracionHistoriaBtn") {
					private static final long serialVersionUID = 8397905807894993988L;

					@Override
					public void onClick() {
						this.setResponsePage(new HistorialIteracionPage(iteracion.getId(), idProyecto));
					}
					
				};
				iteracionHistoriaBtn.setVisible(false);
				
				if(iteracion.getAbierto() == false){
					lblEstimacionHaciendo.setVisible(false);
					lblEStimacionPorHacer.setVisible(false);
					lblEstimacion.setVisible(false);
					lblEstimacionHecho.setVisible(false);
					iteracionBtn.setEnabled(false);
					iteracionHistoriaBtn.setVisible(true);
				}
				
				panel.add(new Link<String>("modificarIteracionBtn"){

					@Override
					public void onClick() {
						this.setResponsePage(new ModificarIteracionPage(iteracion.getId(), idProyecto));
					}
					
				});
				iteracionBtn.add(nombreIteracion);
				panel.add(iteracionBtn);
				panel.add(iteracionHistoriaBtn);
			}
		
		});
		
		this.add(new ListView<UserStorie>("filaUserStories", new PropertyModel<>(this.controller, "userStories")) {

			@Override
			protected void populateItem(ListItem<UserStorie> panel) {
				UserStorie userStorie = panel.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombre", userStorieModel.bind("nombre"));
				Label nombreIteracion = new Label("nombreIteracion", userStorieModel.bind("nombreIteracion"));
				Link userStorieBtn = new Link<String>("userStorieBtn") {
					private static final long serialVersionUID = 8397905807894993988L;
					
					
					@Override
					public void onClick() {
						this.setResponsePage(new DetalleUserStoriePage(userStorie, idProyecto));
					}
				
				};
				
				Form<ControllerBacklogPage> formEliminar = new Form<ControllerBacklogPage>("formEliminar"){
					private static final long serialVersionUID = 1L;

					@Override
					protected void onSubmit() {
						super.onSubmit();
						controller.eliminarUserStorie(userStorie);
						this.setResponsePage(new BacklogPage(idProyecto));
					}
					
				};
				formEliminar.add(new Link<String>("modificarBtn"){

					@Override
					public void onClick() {
						this.setResponsePage(new ModificarUserStoriePage(userStorie.getId(), idProyecto));
					}
					
				});
				
				formEliminar.add(new BotonConfirmar("confirmar", "Usted eliminará la User Storie por completo"));
				panel.add(formEliminar);
			
				userStorieBtn.add(nombreUserStorie);
				panel.add(userStorieBtn);
				panel.add(nombreIteracion);
			}
			
		
		});
		
	
		this.add(new Link<String>("nuevaIteracionBtn"){

			@Override
			public void onClick() {
				this.setResponsePage(new CrearIteracionPage(idProyecto));
			}
			
		});
		
		this.add(new Link<String>("nuevaUserStorieBtn"){

			@Override
			public void onClick() {
				this.setResponsePage(new CrearUserStoriePage(idProyecto));
			}
			
		});
	}
	
	
}
