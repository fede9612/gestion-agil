package ar.edu.unq.sarmiento.epers.home;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.UserStorie;


@Repository
public class ProyectoHome extends AbstractHome<Proyecto>{

	private static final long serialVersionUID = 4775910097257163038L;

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public Proyecto findByName(String name) {
		return this.getSession()
				.createQuery("FROM Maguito WHERE nombre = :name", Proyecto.class)
				.setParameter("name", name)
				.getSingleResult();	
	}

	@Override
	public List<Iteracion> findIteraciones(int proyectoId) {
		return this.getSession()
				.createQuery("FROM Iteracion WHERE proyecto_id = :proyectoId", Iteracion.class)
				.setParameter("proyectoId", proyectoId)
				.getResultList();
	}

	@Override
	public List<UserStorie> findUserStoriesPorHacer(Integer backlogId) {
		return this.getSession()
				.createQuery("FROM UserStorie WHERE backlog_id = :backlogId", UserStorie.class)
				.setParameter("backlogId", backlogId)
				.getResultList();
	}
	
}
