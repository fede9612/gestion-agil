package ar.edu.unq.sarmiento.epers.wicket.usuario;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.wicket.proyecto.HomeProyecto;

public class CrearUsuarioPage extends WebPage{
	
	@SpringBean(name="crearUsuarioController")
	private CrearUsuarioController controller;
	
	public CrearUsuarioPage() {
		super();
		
		Form<CrearUsuarioController> formCrearUsuario = new Form<CrearUsuarioController>("formCrearUsuario"){
			
			@Override
			protected void onSubmit() {
				super.onSubmit();
				controller.persistir();
				this.setResponsePage(HomeProyecto.class);
			}
			
		};
		
		formCrearUsuario.add(new TextField<>("nombreUsuario", new PropertyModel<>(controller, "nombre")));
		this.add(formCrearUsuario);
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new HomeProyecto());
			}
			
			
		});
	}
	
}
