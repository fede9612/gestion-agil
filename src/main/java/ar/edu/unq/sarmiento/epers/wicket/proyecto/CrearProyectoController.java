package ar.edu.unq.sarmiento.epers.wicket.proyecto;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.model.Backlog;
import ar.edu.unq.sarmiento.epers.model.Proyecto;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class CrearProyectoController implements Serializable{

	@Autowired
	private Home<Proyecto> proyectoHome;
	
	private String nombre;
	
	public CrearProyectoController() {
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void persistir(){
		proyectoHome.saveOrUpdate(new Proyecto(this.getNombre(), new Backlog()));
	}
}
