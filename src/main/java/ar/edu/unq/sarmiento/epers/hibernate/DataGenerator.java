package ar.edu.unq.sarmiento.epers.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.home.UsuarioHome;
import ar.edu.unq.sarmiento.epers.home.UsuarioProyectoHome;
import ar.edu.unq.sarmiento.epers.model.Backlog;
import ar.edu.unq.sarmiento.epers.model.Estado;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.Rol;
import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.model.Usuario;
import ar.edu.unq.sarmiento.epers.model.UsuarioProyecto;

@Component
@Transactional
public class DataGenerator {

	@Autowired
	private Home<Proyecto> proyectoHome;
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private UsuarioHome usuarioHome;
	@Autowired
	private UsuarioProyectoHome usuarioProyectoHome;
	
	protected void generate() {
		
		Backlog backlog = new Backlog();
		Proyecto proyecto1 = new Proyecto("Bicicleteria", backlog);
		proyecto1.addRol("Pintor");
		Iteracion iteracion = new Iteracion();
		iteracion.setNombre("Inicio del proyecto");
		iteracion.setAbierto(true);
		proyecto1.addIteraciones(iteracion);
		UserStorie userStorie1 = new UserStorie();
		userStorie1.setNombre("Crear DER");
		userStorie1.setEstimacion(8);
		UserStorie userStorie2 = new UserStorie();
		userStorie2.setNombre("Crear Modelo de Objetos");
		userStorie2.setDescripcion("Se crean todas las entidades");
		userStorie1.setIteracion(iteracion);
		userStorie1.setEstado(Estado.PorHacer);
		userStorie2.setEstado(Estado.PorHacer);
		userStorie2.setIteracion(null);
		backlog.addUserStorie(userStorie1);
		backlog.addUserStorie(userStorie2);
		
		Iteracion iteracion2 = new Iteracion("Stage 2");
		iteracion2.setAbierto(true);
		proyecto1.addIteraciones(iteracion2);
		UserStorie userStorie3 = new UserStorie();
		userStorie3.setNombre("Modificar Home Page");
		userStorie3.setIteracion(iteracion2);
		userStorie3.setEstado(Estado.PorHacer);
		backlog.addUserStorie(userStorie3);
		
		Usuario user1 = new Usuario("Ramón Lopez");
		Usuario user2 = new Usuario("Rodrigo Pereira");
		Usuario user3 = new Usuario("Matías Rodriguez");
		
		Transaction ts = sessionFactory.getCurrentSession().beginTransaction();
		proyectoHome.saveOrUpdate(proyecto1);
		usuarioHome.saveOrUpdate(user1);
		usuarioHome.saveOrUpdate(user2);
		usuarioHome.saveOrUpdate(user3);
		UsuarioProyecto user1Proyecto1 = new UsuarioProyecto(proyecto1, user1, Rol.dueño);
		usuarioProyectoHome.saveOrUpdate(user1Proyecto1);
		ts.commit();
	}
}
