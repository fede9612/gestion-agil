package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;


public class ModificarUserStoriePage extends WebPage{

	@SpringBean(name="modificarUserStorieController")
	private ModificarUserStorieController controller;
	
	public ModificarUserStoriePage(int idUserStorie,int idProyecto) {
		super();
		controller.setProyectoId(idProyecto);
		controller.setUserStorieId(idUserStorie);
		controller.cargarAtributos();
		
		Form<CrearUserStorieController> formModificarUserStorie = new Form<CrearUserStorieController>("formModificarUserStorie"){
			
			@Override
			protected void onSubmit() {
				super.onSubmit();
				controller.modificar();
				this.setResponsePage(new ModificarUserStoriePage(idUserStorie,idProyecto));
			}
			
		};
		
		DropDownChoice<String> dropChoice = new DropDownChoice<>(
				// id
				"iteracion",
				// binding del valor
				new PropertyModel<>(controller, "iteracion"),
				// binding de la lista de items
				new PropertyModel<>(controller, "iteraciones"),
				// que se muestra de cada item
				new ChoiceRenderer<>("nombre"));
		
		formModificarUserStorie.add(new DropDownChoice<>(
				// id
				"rol",
				// binding del valor
				new PropertyModel<>(controller, "rol"),
				// binding de la lista de items
				new PropertyModel<>(controller, "roles"),
				// que se muestra de cada item
				new ChoiceRenderer<>("toString")));
		
		formModificarUserStorie.add(new Link<String>("quitarIteracionBtn"){

			@Override
			public void onClick() {
				controller.quitarIteracion();
			}
			
			
		});
		formModificarUserStorie.add(dropChoice);
		formModificarUserStorie.add(new Label("descripcion", new PropertyModel<>(controller, "descripcionActual")));
		formModificarUserStorie.add(new TextField<>("nombreUserStorie", new PropertyModel<>(controller, "nombre")));
		formModificarUserStorie.add(new TextArea<>("descripcionUserStorie", new PropertyModel<>(controller, "descripcion")));
		formModificarUserStorie.add(new TextArea<>("paraUserStorie", new PropertyModel<>(controller, "para")));
		this.add(formModificarUserStorie);
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(idProyecto));
			}
			
			
		});
	}	
	
}
