package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.model.Estado;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class DetalleIteracionController implements Serializable{

	@Autowired
	private IteracionHome iteracionHome;
	@Autowired
	private UserStorieHome userStorieHome;
	private int idIteracion;
	
	public void setIdIteracion(int idIteracion) {
		this.idIteracion = idIteracion;
	}

	public int getIdIteracion() {
		return idIteracion;
	}
	
	public Iteracion getIteracion(){
		return iteracionHome.find(this.getIdIteracion());
	}

	public List<UserStorie> getUserStories(){
		return iteracionHome.findUserStoriesPorHacer(this.getIdIteracion());
	}
	
	public List<UserStorie> getUserStoriesHaciendo(){
		return iteracionHome.findUserStoriesHaciendo(this.getIdIteracion());
	}
	
	public List<UserStorie> getUserStoriesHecho(){
		return iteracionHome.findUserStoriesHecho(this.getIdIteracion());
	}

	public void eliminar(UserStorie userStorie) {
		userStorie.setIteracion(null);
		userStorieHome.saveOrUpdate(userStorie);
	}

	public String estimacion(UserStorie userStorie) {
		UserStorie storie = userStorieHome.find(userStorie.getId());
		return Integer.toString(storie.getEstimacion());
	}

	public void cerrarIteracion() {
		Iteracion iteracion = this.getIteracion();
		this.guardarHistorial(iteracion);
		this.liberarUserStoriesSinHacerse(iteracion);
		iteracionHome.saveOrUpdate(iteracion);
	}

	private void liberarUserStoriesSinHacerse(Iteracion iteracion) {
		this.getUserStories().forEach(u -> u.setIteracion(null));
		//En el siguiente no pude volver a usar el forEach para recorrer la lista 
		//porque tenía que hacer dos acciones por cada elemento
		int i = 0;
		List<UserStorie> list = new ArrayList<>(this.getUserStoriesHaciendo());
		while(i < list.size()){
			list.get(i).setEstado(Estado.PorHacer);
			list.get(i).setIteracion(null);
			i++;
		}
	}

	private void guardarHistorial(Iteracion iteracion) {
		iteracion.setAbierto(false);
		iteracion.setUserStoriesPorHacer(this.userStoriesIdPorHacer());
		iteracion.setUserStoriesHaciendo(this.userStoriesIdHaciendo());
		iteracion.setUserStoriesHecho(this.userStoriesIdHecho());
	}

	private List<Integer> userStoriesIdHecho() {
		return this.getUserStoriesHecho().stream().map(u -> u.getId()).collect(Collectors.toList());
	}

	private List<Integer> userStoriesIdHaciendo() {
		return this.getUserStoriesHaciendo().stream().map(u -> u.getId()).collect(Collectors.toList());
	}

	private List<Integer> userStoriesIdPorHacer() {
		return this.getUserStories().stream().map(u -> u.getId()).collect(Collectors.toList());
	}
	
}
