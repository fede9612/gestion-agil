package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.ProyectoHome;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class ModificarIteracionController implements Serializable{

	@Autowired
	private IteracionHome iteracionHome;
	private int iteracionId;
	private int proyectoId;
	private String nombre;
	
	public void modificar(){
		Iteracion iteracion = this.getIteracion();
		iteracion.setNombre(this.getNombre());
		iteracionHome.saveOrUpdate(iteracion);
	}
	
	public void eliminar(){
		iteracionHome.delete(this.getIteracion());
	}
	
	public void cargarDatos(){
		this.nombre = this.getIteracion().getNombre();
	}
	
	public Iteracion getIteracion(){
		return iteracionHome.find(iteracionId);
	}

	public int getIteracionId() {
		return iteracionId;
	}

	public void setIteracionId(int iteracionId) {
		this.iteracionId = iteracionId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getProyectoId() {
		return proyectoId;
	}

	public void setProyectoId(int proyectoId) {
		this.proyectoId = proyectoId;
	}
	
}
