package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.home.UsuarioHome;
import ar.edu.unq.sarmiento.epers.model.Estado;
import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.model.Usuario;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.ConfigurarProyectoController;

@Service
@Scope(value= ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class ConfigurarUserStorieController implements Serializable{

	@Autowired
	private UserStorieHome userStorieHome;
	@Autowired
	private UsuarioHome usuarioHome;
	@Autowired
	private ConfigurarProyectoController configurarProyectoController;
	private int userStorieId;
	private int proyectoId;
	private Usuario usuario;
	private int estimacion;

	public int getUserStorieId() {
		return userStorieId;
	}

	public void setUserStorieId(int userStorieId) {
		this.userStorieId = userStorieId;
	}
	
	public String getNombre(){
		return this.getUserStorie().getNombre();
	}
	
	public String getEstado(){
		return this.getUserStorie().getEstado().getString();
	}
		
	private UserStorie getUserStorie(){
		return userStorieHome.find(this.getUserStorieId());
	}

	public void cambiarEstado(Estado estado) {
		UserStorie userStorie = this.getUserStorie();
		userStorie.setEstado(estado);
		userStorieHome.saveOrUpdate(userStorie);
	}
	
	public List<Usuario> getUsuariosParaUnProyecto(){
		return configurarProyectoController.getUsuariosParaUnProyecto(this.getProyectoId());
	}

	public int getProyectoId() {
		return proyectoId;
	}

	public void setProyectoId(int proyectoId) {
		this.proyectoId = proyectoId;
	}

	public void persistirUsuario() {
		UserStorie userStorie = this.getUserStorie();
		userStorie.setUsuario(this.getUsuario());
		userStorieHome.saveOrUpdate(userStorie);
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getNombreUsuario(){
		if(!(this.getUserStorie().getUsuario() == null)){
			return this.getUserStorie().getUsuario().getNombre();			
		}else{
			return "Ninguno";
		}
	}

	public void persistirEstimacion() {
		UserStorie userStorie = this.getUserStorie();
		userStorie.setEstimacion(this.getEstimacion());
		userStorieHome.saveOrUpdate(userStorie);
	}

	public int getEstimacion() {
		return estimacion;
	}

	public void setEstimacion(int estimacion) {
		this.estimacion = estimacion;
	}
	
	public int getEstimacionActual(){
		return this.getUserStorie().getEstimacion();
	}
	
}
