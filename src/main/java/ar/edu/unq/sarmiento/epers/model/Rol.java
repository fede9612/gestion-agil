package ar.edu.unq.sarmiento.epers.model;

public enum Rol {
	
	dueño("Dueño"), administrador("Administrador"), desarrollador("Desarrollador");
	
	private String rol;
	
	Rol(String rol){
		this.rol = rol;
	}
	
	public String getString(){
		return this.rol;
	}
}
