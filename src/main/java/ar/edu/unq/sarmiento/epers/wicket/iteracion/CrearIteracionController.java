package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class CrearIteracionController implements Serializable{

	@Autowired
	private Home<Iteracion> proyectoHome;
	private String nombre;
	private Integer idProyecto;
	
	public void persistir() {
		Iteracion iteracion = new Iteracion(getNombre());
		iteracion.setAbierto(true);
		Proyecto proyecto = proyectoHome.findById(this.getIdProyecto());
		iteracion.setProyecto(proyecto);
		proyectoHome.saveOrUpdate(iteracion);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(Integer idProyecto) {
		this.idProyecto = idProyecto;
	}

}
