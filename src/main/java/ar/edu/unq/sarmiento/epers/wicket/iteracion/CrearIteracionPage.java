package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.HomeProyecto;

public class CrearIteracionPage extends WebPage{

	@SpringBean(name="crearIteracionController")
	private CrearIteracionController controller;
	
	public CrearIteracionPage(int idProyecto) {
		super();
		controller.setIdProyecto(idProyecto);
	Form<CrearIteracionController> formCrearIteracion = new Form<CrearIteracionController>("formCrearIteracion"){
			
			@Override
			protected void onSubmit() {
				super.onSubmit();
				controller.persistir();
				this.setResponsePage(new BacklogPage(idProyecto));
			}
			
		};
		
		formCrearIteracion.add(new TextField<>("nombreIteracion", new PropertyModel<>(controller, "nombre")));
		this.add(formCrearIteracion);
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(idProyecto));
			}
			
			
		});
	}
	
	
}
