package ar.edu.unq.sarmiento.epers.wicket.proyecto;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.UsuarioProyecto;
import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;

public class ConfiguracionProyectoPage extends WebPage{
	private static final long serialVersionUID = 1L;

	@SpringBean(name="configurarProyectoController")
	private ConfigurarProyectoController controller;
	
	public ConfiguracionProyectoPage(int idProyecto) {
		super();
		controller.setIdProyecto(idProyecto);
		Form<ConfigurarProyectoController> formConfiguracion = new Form<ConfigurarProyectoController>("formConfiguracion"){
			private static final long serialVersionUID = 1L;
			
			
		};
		
		DropDownChoice<String> dropChoiceUsuarios = new DropDownChoice<>(
				// id
				"usuarios",
				// binding del valor
				new PropertyModel<>(controller, "usuario"),
				// binding de la lista de items
				new PropertyModel<>(controller, "usuarios"),
				// que se muestra de cada item
				new ChoiceRenderer<>("nombre"));
		
		DropDownChoice<String> dropChoiceRoles = new DropDownChoice<>(
				// id
				"roles",
				// binding del valor
				new PropertyModel<>(controller, "rol"),
				// binding de la lista de items
				new PropertyModel<>(controller, "roles"),
				// que se muestra de cada item
				new ChoiceRenderer<>("string"));

		Button eliminarBtn = new Button("okbutton"){
            public void onSubmit() {
                controller.eliminar();
				this.setResponsePage(new HomeProyecto());
            }
        };
		Button agregarUsuarioBtn = new Button("agregarUsuarioBtn"){
            public void onSubmit() {
                controller.agregarUsuario(idProyecto);
                this.setResponsePage(new ConfiguracionProyectoPage(idProyecto));
            }
        };
        
        Button agregarRolBtn = new Button("agregarRolBtn"){
            public void onSubmit() {
                controller.agregarRol();
                this.setResponsePage(new ConfiguracionProyectoPage(idProyecto));
            }
        };
        
        TextField<String> rolUserStorie = new TextField<String>("nombreRol", new PropertyModel<>(controller, "rolUserStorie"));
        
        ListView<UsuarioProyecto> listUsuario = new ListView<UsuarioProyecto>("usuariosEnProyecto", new PropertyModel<>(controller, "usuariosEnProyecto")) {

			@Override
			protected void populateItem(ListItem<UsuarioProyecto> item) {
				UsuarioProyecto usuarioProyecto = item.getModelObject();
				CompoundPropertyModel<UsuarioProyecto> proyectoModel = new CompoundPropertyModel<>(usuarioProyecto);
				Label nombreUsuario = new Label("nombre", proyectoModel.bind("usuario.nombre"));
				Label rolUsuario = new Label("rol", proyectoModel.bind("rol.string"));
				item.add(nombreUsuario);
				item.add(rolUsuario);
				item.add(new Link<String>("quitarUsuario"){

					@Override
					public void onClick() {
						controller.quitarUsuario(usuarioProyecto.getId());
		                this.setResponsePage(new ConfiguracionProyectoPage(idProyecto));
					}
					
				});
			}
		};
		
		this.add(new Link<String>("volver") {

			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(idProyecto));
			}
		});
        
        formConfiguracion.add(eliminarBtn);
        formConfiguracion.add(agregarUsuarioBtn);
        formConfiguracion.add(agregarRolBtn);
        formConfiguracion.add(dropChoiceUsuarios);
        formConfiguracion.add(dropChoiceRoles);
        formConfiguracion.add(rolUserStorie);
        formConfiguracion.add(listUsuario);
		this.add(formConfiguracion);
	}
	
}
