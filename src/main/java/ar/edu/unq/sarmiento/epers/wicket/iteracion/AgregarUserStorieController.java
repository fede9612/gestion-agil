package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.ProyectoHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class AgregarUserStorieController implements Serializable{
	
	@Autowired
	private UserStorieHome userStorieHome;
	@Autowired
	private ProyectoHome proyectoHome;
	@Autowired
	private IteracionHome iteracionHome;
	private int proyectoId;
	private int iteracionId;
	private UserStorie userStorie;
	
	
	public int getProyectoId() {
		return proyectoId;
	}
	public void setProyectoId(int proyectoId) {
		this.proyectoId = proyectoId;
	}
	public int getIteracionId() {
		return iteracionId;
	}
	public void setIteracionId(int iteracionId) {
		this.iteracionId = iteracionId;
	}
	public UserStorie getUserStorie() {
		return userStorie;
	}
	public void setUserStorie(UserStorie userStorie) {
		this.userStorie = userStorie;
	}
	
	public Iteracion getIteracion(){
		return iteracionHome.find(this.getIteracionId());
	}
	
	public Proyecto getProyecto(){
		return proyectoHome.findById(this.getProyectoId());
	}
	
	public List<UserStorie> getUserStorieSinAsignar(){
		return this.getUserStories();
	}
	
	public List<UserStorie> getUserStories(){
		return userStorieHome.getUserStoriesSinIteracionParaUn(this.getProyecto().getBacklog());
	}
	
	public List<UserStorie> getUserStorieAsignadas(){
		return this.getIteracion().getUserStories();
	}
	
	public void agregarIteracio(UserStorie userStorie) {
		Iteracion iteracion = this.getIteracion();
		iteracion.addUserStorie(userStorie);
		iteracionHome.saveOrUpdate(iteracion);
	}
	
	public String getNombreIteracion(){
		return this.getIteracion().getNombre();
	}
	
	public void quitarUserStorie(UserStorie userStorie){
		userStorie.setIteracion(null);
		userStorieHome.saveOrUpdate(userStorie);
	}
}
