package ar.edu.unq.sarmiento.epers.wicket.proyecto;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class CrearProyectoPage extends WebPage{
	
	@SpringBean(name="crearProyectoController")
	private CrearProyectoController controller;
	
	public CrearProyectoPage() {
		super();
		
		Form<CrearProyectoController> formCrearProyecto = new Form<CrearProyectoController>("formCrearProyecto"){
			
			@Override
			protected void onSubmit() {
				super.onSubmit();
				controller.persistir();
				this.setResponsePage(HomeProyecto.class);
			}
			
		};
		
		formCrearProyecto.add(new TextField<>("nombreProyecto", new PropertyModel<>(controller, "nombre")));
		this.add(formCrearProyecto);
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new HomeProyecto());
			}
			
			
		});
	}
	
}
