package ar.edu.unq.sarmiento.epers.wicket.proyecto;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;
import ar.edu.unq.sarmiento.epers.wicket.usuario.CrearUsuarioPage;

public class HomeProyecto extends WebPage{
	
	private static final long serialVersionUID = 1L;
	@SpringBean(name="homeProyectoController")
	private HomeProyectoController controller;
	
	public HomeProyecto(){
		super();
		
		this.add(new ListView<Proyecto>("filaProyecto", new PropertyModel<>(this.controller, "proyectos")) {

			private static final long serialVersionUID = 2426749934569985837L;

			protected void populateItem(ListItem<Proyecto> panel) {
				Proyecto proyecto = panel.getModelObject();
				CompoundPropertyModel<Proyecto> proyectoModel = new CompoundPropertyModel<>(proyecto);
				Label nombreProyecto = new Label("nombre", proyectoModel.bind("nombre"));
				
				Link verProyecto = new Link<String>("verProyecto") {
					private static final long serialVersionUID = 8397905807894993988L;
					
					
					@Override
					public void onClick() {
						this.setResponsePage(new BacklogPage(proyecto.getId()));
					}
					
				};
				
				verProyecto.add(nombreProyecto);
				panel.add(verProyecto);
			};
			
		});
		
		this.add(new Link<String>("nuevoUsuario"){

			@Override
			public void onClick() {
				this.setResponsePage(new CrearUsuarioPage());
			}
			
		});
		
		Link nuevoProyecto = new Link<String>("nuevoProyecto") {
			
			@Override
			public void onClick() {
				this.setResponsePage(new CrearProyectoPage());
			}
		};
		
		this.add(nuevoProyecto);
	}
}
