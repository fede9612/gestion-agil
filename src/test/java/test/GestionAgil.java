package test;
import static org.junit.Assert.*;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ar.edu.unq.sarmiento.epers.hibernate.HibernateConf;
import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.ProyectoHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.home.UsuarioHome;
import ar.edu.unq.sarmiento.epers.home.UsuarioProyectoHome;
import ar.edu.unq.sarmiento.epers.model.Backlog;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.Rol;
import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.model.Usuario;
import ar.edu.unq.sarmiento.epers.model.UsuarioProyecto;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConf.class})
@Transactional
public class GestionAgil {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private ProyectoHome proyectoHome;
	@Autowired
	private UsuarioHome usuarioHome;
	@Autowired
	private IteracionHome iteracionHome;
	@Autowired
	private UserStorieHome userStorieHome;
	@Autowired
	private UsuarioProyectoHome usuarioProyectoHome;
	
	private Proyecto proyecto1;	
	private Usuario usuario1;
	private Iteracion iteracion1;
	private UserStorie userStorie1;
	private UsuarioProyecto usuarioProyecto1;
	
	@Before
    public void before() {
		HibernateConf.modo = "generate";
		proyecto1 = new Proyecto("Relojes", new Backlog());
		usuario1 = new Usuario("Felipe Perez");
		iteracion1 = new Iteracion("Inicio del proyecto");
		proyecto1.addIteraciones(iteracion1);
		userStorie1 = new UserStorie();
		usuarioProyecto1 = new UsuarioProyecto(proyecto1, usuario1, Rol.dueño);
    }
	
	
	@Test
	public void persistirProyectoRelojes(){
		proyectoHome.saveOrUpdate(proyecto1);	
		assertNotNull(proyectoHome.find(proyecto1.getId()));
	}
	
	@Test
	public void cambiarNombreProyectoRelojesARelojitos(){
		proyecto1.setNombre("Relojitos");
		proyectoHome.saveOrUpdate(proyecto1);
		assertEquals("Relojitos", proyectoHome.find(proyecto1.getId()).getNombre());
	}
	
	@Test
	public void persistirUsuario(){
		usuarioHome.saveOrUpdate(usuario1);
		assertNotNull(usuarioHome.findByIdUsuario(usuario1.getId()));
	}
	
	@Test
	public void persistirIteracion(){
		iteracionHome.saveOrUpdate(iteracion1);
		assertNotNull(iteracionHome.find(iteracion1.getId()));
	}
	
	@Test
	public void persistirUserStorie(){
		userStorieHome.saveOrUpdate(userStorie1);
		assertNotNull(userStorieHome.find(userStorie1.getId()));
	}
	
	@Test
	public void persistirUsuario1EnProyecto1(){
		usuarioProyectoHome.saveOrUpdate(usuarioProyecto1);
		assertNotNull(usuarioProyectoHome.find(usuarioProyecto1.getId()));
	}
	
	
}
