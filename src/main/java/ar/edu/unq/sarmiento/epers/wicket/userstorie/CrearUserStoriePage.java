package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;

public class CrearUserStoriePage extends WebPage{
	
	@SpringBean(name="crearUserStorieController")
	private CrearUserStorieController controller;
	
	public CrearUserStoriePage(int idProyecto) {
		super();
		
		controller.setIdProyecto(idProyecto);
		Form<CrearUserStorieController> formCrearUserStorie = new Form<CrearUserStorieController>("formCrearUserStorie"){
				
				@Override
				protected void onSubmit() {
					super.onSubmit();
					controller.persistir();
					this.setResponsePage(new BacklogPage(idProyecto));
				}
				
			};
			
			formCrearUserStorie.add(new DropDownChoice<>(
					// id
					"iteracion",
					// binding del valor
					new PropertyModel<>(controller, "iteracion"),
					// binding de la lista de items
					new PropertyModel<>(controller, "iteraciones"),
					// que se muestra de cada item
					new ChoiceRenderer<>("nombre")));
			
			formCrearUserStorie.add(new DropDownChoice<>(
					// id
					"rol",
					// binding del valor
					new PropertyModel<>(controller, "rol"),
					// binding de la lista de items
					new PropertyModel<>(controller, "roles"),
					// que se muestra de cada item
					new ChoiceRenderer<>("toString")));
			
			formCrearUserStorie.add(new TextField<>("nombreUserStorie", new PropertyModel<>(controller, "nombre")));
			formCrearUserStorie.add(new TextArea<>("descripcionUserStorie", new PropertyModel<>(controller, "descripcion")));
			formCrearUserStorie.add(new TextArea<>("paraUserStorie", new PropertyModel<>(controller, "para")));
			this.add(formCrearUserStorie);
			this.add(new Link<String>("volver"){

				@Override
				public void onClick() {
					this.setResponsePage(new BacklogPage(idProyecto));
				}
				
				
			});
		}	
}
