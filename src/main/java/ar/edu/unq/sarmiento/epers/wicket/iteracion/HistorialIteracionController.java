package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class HistorialIteracionController implements Serializable{

	@Autowired
	private IteracionHome iteracionHome;
	@Autowired
	private UserStorieHome userStorieHome;
	private int iteracionId;
	
	public int getIteracionId() {
		return iteracionId;
	}
	
	public void setIteracionId(int iteracionId) {
		this.iteracionId = iteracionId;
	}
	
	public List<UserStorie> getUserStoriesHecho(){
		return this.buscarUserStories(iteracionHome.findHistoriaUserStoriesHecho(this.getIteracionId()));
	}
	
	public List<UserStorie> getUserStoriesHaciendo(){
		return this.buscarUserStories(iteracionHome.findHistoriaUserStoriesHaciendo(this.getIteracionId()));
	}
	
	public List<UserStorie> getUserStoriesPorHacer(){
		return this.buscarUserStories(iteracionHome.findHistoriaUserStoriesPorHacer(this.getIteracionId()));
	}

	private List<UserStorie> buscarUserStories(List<Integer> listHistoriaUserStorie) {
		int i = 0;
		List<Integer> list = new ArrayList<>(listHistoriaUserStorie);
		List<UserStorie> listUserStorie = new ArrayList<>();
		while(i < list.size()){
			listUserStorie.add(userStorieHome.find(list.get(i)));
			i++;
		}
		return listUserStorie;
	}
	
	public int getEstimacionTotal(){
		return this.getUserStoriesHaciendo().stream().mapToInt(u -> u.getEstimacion()).sum() +
			   this.getUserStoriesPorHacer().stream().mapToInt(u -> u.getEstimacion()).sum() +
			   this.getUserStoriesHecho().stream().mapToInt(u -> u.getEstimacion()).sum();
	}
	
	public int getEstimacionTerminada(){
		return this.getUserStoriesHecho().stream().mapToInt(u -> u.getEstimacion()).sum();
	}
	
	public int getEstimacionNoTerminada(){
		return this.getUserStoriesHaciendo().stream().mapToInt(u -> u.getEstimacion()).sum() +
		       this.getUserStoriesPorHacer().stream().mapToInt(u -> u.getEstimacion()).sum();
	}

	
}
