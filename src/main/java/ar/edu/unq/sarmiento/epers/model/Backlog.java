package ar.edu.unq.sarmiento.epers.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Backlog extends Persistible{
	
	private String nombre = "Backlog";
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="backlog_id")
	private List<UserStorie> stories = new ArrayList<>();
	
	public void addUserStorie(UserStorie storie){
		this.stories.add(storie);
		storie.setBacklog(this);
	}
	
	public void rmUserStorie(UserStorie storie){
		this.stories.remove(storie);
	}
}
