package ar.edu.unq.sarmiento.epers.home;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Restrictions;
import org.hibernate.query.criteria.internal.compile.CriteriaQueryTypeQueryAdapter;
import org.springframework.stereotype.Component;

import ar.edu.unq.sarmiento.epers.model.Estado;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Component
public class IteracionHome extends AbstractHome<Iteracion>{

	@Override
	public Iteracion findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Iteracion> findIteraciones(int proyectoId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<UserStorie> findUserStories(Integer idIteracion){
		return this.getSession()
				.createQuery("FROM UserStorie WHERE iteracion_id = :idIteracion", UserStorie.class)
				.setParameter("idIteracion", idIteracion)
				.getResultList();
	}

	@Override
	public List<UserStorie> findUserStoriesPorHacer(Integer idIteracion) {
		/*return this.getSession()
				.createNamedQuery("storiesIteracion", UserStorie.class)
				.setParameter("idIteracion", idIteracion)
				.setParameter("estado", Estado.PorHacer)
				.getResultList();*/
		/*return this.getSession().createCriteria(UserStorie.class)
				.add(Restrictions.eq("iteracion_id", idIteracion))
				.add(Restrictions.eq("estado", Estado.PorHacer))
				.list();*/
//		CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
//		CriteriaQuery<UserStorie> criteria = builder.createQuery(UserStorie.class);
//		Root<UserStorie> root = criteria.from(UserStorie.class);
//		criteria.select(root);
//		criteria.where(builder.equal(root.get("iteracion_id"), idIteracion),
//					   builder.equal(root.get("estado"), Estado.PorHacer));
//		return getSession().createQuery(criteria).list();
		return this.getSession()
				.createNamedQuery("storiesIteracion", UserStorie.class)
				.setParameter("idIteracion", idIteracion)
				.setParameter("estado", Estado.PorHacer)
				.getResultList();
	}

	public List<UserStorie> findUserStoriesHaciendo(int idIteracion) {
		return this.getSession()
				.createNamedQuery("storiesIteracion", UserStorie.class)
				.setParameter("idIteracion", idIteracion)
				.setParameter("estado", Estado.Haciendo)
				.getResultList();
	}
	
	public List<UserStorie> findUserStoriesHecho(int idIteracion) {
		return this.getSession()
				.createNamedQuery("storiesIteracion", UserStorie.class)
				.setParameter("idIteracion", idIteracion)
				.setParameter("estado", Estado.Hecho)
				.getResultList();
	}
	
	public List<Integer> findHistoriaUserStoriesHecho(int idIteracion){
		return this.getSession()
				.createSQLQuery("SELECT user_story FROM UserStoryHecho WHERE iteracion_id = :idIteracion")
				.setParameter("idIteracion", idIteracion)
				.getResultList();
	}
	
	public List<Integer> findHistoriaUserStoriesHaciendo(int idIteracion){
		return this.getSession()
				.createSQLQuery("SELECT user_story FROM UserStoryHaciendo WHERE iteracion_id = :idIteracion")
				.setParameter("idIteracion", idIteracion)
				.getResultList();
	}
	
	public List<Integer> findHistoriaUserStoriesPorHacer(int idIteracion){
		return this.getSession()
				.createSQLQuery("SELECT user_story FROM UserStoryPorHacer WHERE iteracion_id = :idIteracion")
				.setParameter("idIteracion", idIteracion)
				.getResultList();
	}
}
