package ar.edu.unq.sarmiento.epers.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Proyecto extends Persistible{
	
	private String nombre;
	@OneToOne(cascade=CascadeType.ALL)
	private Backlog backlog;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="proyecto_id")
	@Column(nullable = true)
	private List<Iteracion> iteraciones = new ArrayList<>();
	@ElementCollection
	@CollectionTable(name="Rol", joinColumns=@JoinColumn(name="proyecto_id"))
	@Column(name="nombre_rol")
	private List<String> roles = new ArrayList<>();
	
	public Proyecto(){
	}
	
	public Proyecto(String nombre, Backlog backlog){
		this.nombre = nombre;
		this.backlog = backlog;
	}
	
	public void addRol(String rol){
		this.roles.add(rol);
	}
	
	public List<String> getRoles(){
		return this.roles;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Backlog getBacklog() {
		return backlog;
	}
	
	public void setBacklog(Backlog backlog) {
		this.backlog = backlog;
	}
	
	public List<Iteracion> getIteraciones() {
		return iteraciones;
	}
	
	public void addIteraciones(Iteracion iteracion) {
		this.iteraciones.add(iteracion);
		iteracion.setProyecto(this);
	}
	
}
