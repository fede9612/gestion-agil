package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;
import ar.edu.unq.sarmiento.epers.wicket.userstorie.ConfigurarUserStoriePage;
import ar.edu.unq.sarmiento.epers.wicket.userstorie.DetalleUserStoriePage;

public class DetalleIteracionPage extends WebPage{

	@SpringBean(name="detalleIteracionController")
	private DetalleIteracionController controller;
	
	public DetalleIteracionPage(int idProyecto, int idIteracion) {
		super();
		this.controller.setIdIteracion(idIteracion);
		
		this.add(new Link<String>("agregarUserStorieBtn"){

			@Override
			public void onClick() {
				this.setResponsePage(new AgregarUserStoriePage(idProyecto, idIteracion));
			}
			
		});
		
		this.add(new Link<String>("cerrarIteracionBtn"){

			@Override
			public void onClick() {
				controller.cerrarIteracion();
				this.setResponsePage(new HistorialIteracionPage(idIteracion, idProyecto));
			}
			
		});
		
		this.add(new ListView<UserStorie>("filaUserStories", new PropertyModel<>(this.controller, "userStories")) {

			@Override
			protected void populateItem(ListItem<UserStorie> panel) {
				UserStorie userStorie = panel.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombre", userStorieModel.bind("nombre"));
				Link userStorieBtn = new Link<String>("userStorieBtn") {
					private static final long serialVersionUID = 8397905807894993988L;
					
					
					@Override
					public void onClick() {
						this.setResponsePage(new ConfigurarUserStoriePage(userStorie.getId(), idProyecto, idIteracion));
					}
					
				};
				panel.add(new Label("estimacion", controller.estimacion(userStorie)));
				panel.add(new Link<String>("borrarUserStorie"){
					@Override
					public void onClick() {
						controller.eliminar(userStorie);
					}
				});
							
				userStorieBtn.add(nombreUserStorie);
				panel.add(userStorieBtn);
			}
			
		
		});
		
		this.add(new ListView<UserStorie>("filaUserStoriesHaciendo", new PropertyModel<>(this.controller, "userStoriesHaciendo")) {

			@Override
			protected void populateItem(ListItem<UserStorie> panel) {
				UserStorie userStorie = panel.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombre", userStorieModel.bind("nombre"));
				Link userStorieBtn = new Link<String>("userStorieBtn") {
					private static final long serialVersionUID = 8397905807894993988L;
					
					
					@Override
					public void onClick() {
						this.setResponsePage(new ConfigurarUserStoriePage(userStorie.getId(),idProyecto, idIteracion));					
					}
				};
				panel.add(new Label("estimacion2", controller.estimacion(userStorie)));
				panel.add(new Link<String>("borrarUserStorie"){
					@Override
					public void onClick() {
						controller.eliminar(userStorie);
					}
				});
							
				userStorieBtn.add(nombreUserStorie);
				panel.add(userStorieBtn);
			}
			
		
		});
		
		this.add(new ListView<UserStorie>("filaUserStoriesHecho", new PropertyModel<>(this.controller, "userStoriesHecho")) {

			@Override
			protected void populateItem(ListItem<UserStorie> panel) {
				UserStorie userStorie = panel.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombre", userStorieModel.bind("nombre"));
				Link userStorieBtn = new Link<String>("userStorieBtn") {
					private static final long serialVersionUID = 8397905807894993988L;
					
					
					@Override
					public void onClick() {
						this.setResponsePage(new ConfigurarUserStoriePage(userStorie.getId(),idProyecto, idIteracion));
					}
					
				};
				panel.add(new Label("estimacion3", controller.estimacion(userStorie)));
				panel.add(new Link<String>("borrarUserStorie"){
					@Override
					public void onClick() {
						controller.eliminar(userStorie);
					}
				});
							
				userStorieBtn.add(nombreUserStorie);
				panel.add(userStorieBtn);
			}
			
		
		});
		
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(idProyecto));
			}
			
		});
	}
	
}
