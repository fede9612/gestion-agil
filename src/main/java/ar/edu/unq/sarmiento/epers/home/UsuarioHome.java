package ar.edu.unq.sarmiento.epers.home;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.model.Usuario;

@Component
public class UsuarioHome extends AbstractHome<Usuario>{

	@Override
	public Usuario findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Iteracion> findIteraciones(int proyectoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserStorie> findUserStoriesPorHacer(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public Usuario findByIdUsuario(int id){
		return this.getSession().load(Usuario.class, id);
	}
	
	public List<Usuario> findUsuarios(){
		return this.getSession().createQuery("FROM Usuario").getResultList();
	}

	
}
