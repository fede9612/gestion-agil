package ar.edu.unq.sarmiento.epers;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import ar.edu.unq.sarmiento.epers.hibernate.HibernateConf;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.HomeProyecto;

/**
 * Application object for your web application.
 * If you want to run this application without deploying, run the Start class.
 * 
 * @see ar.edu.unq.sarmiento.epers.Start#main(String[])
 */
@Configuration
public class WicketApplication extends WebApplication
{
	@Autowired
	private SessionFactory sessionFactory;
	private static ApplicationContext applicationContext;
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends WebPage> getHomePage()
	{
		return HomeProyecto.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();		
		HibernateConf.modo = "server";
		// Acá se integra Wicket con Spring
		getComponentInstantiationListeners().add(new SpringComponentInjector(this));
		
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
}
