package ar.edu.unq.sarmiento.epers.wicket.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.UsuarioHome;
import ar.edu.unq.sarmiento.epers.model.Usuario;

@Service
@Scope(value= ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class CrearUsuarioController {

	@Autowired
	private UsuarioHome usuarioHome;
	private String nombre;
	
	public void persistir() {
		usuarioHome.saveOrUpdate(new Usuario(this.getNombre()));
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
