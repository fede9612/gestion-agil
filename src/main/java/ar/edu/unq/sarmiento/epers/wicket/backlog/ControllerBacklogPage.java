package ar.edu.unq.sarmiento.epers.wicket.backlog;

import java.io.Serializable;
import java.util.List;

import org.apache.wicket.model.IModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.home.IteracionHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class ControllerBacklogPage implements Serializable{
	
	@Autowired
	private Home<Proyecto> proyectoHome;
	@Autowired
	private UserStorieHome userStorieHome;
	@Autowired
	private IteracionHome iteracionHome;
	
	private int idProyecto;
	
	public ControllerBacklogPage() {
	}

	public int getIdProyecto() {
		return idProyecto;
	}
	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}
	
	public Proyecto getProyecto(){		
		return proyectoHome.findById(getIdProyecto());
	}
	
	public List<Iteracion> getIteraciones(){
		return proyectoHome.findIteraciones(this.getProyecto().getId());
	}
	
	public List<UserStorie> getUserStories(){
		return proyectoHome.findUserStoriesPorHacer(this.getProyecto().getId());
	}

	public void eliminarUserStorie(UserStorie userStorie) {
		userStorieHome.delete(userStorie);
	}

	public void eliminarIteracion(Iteracion iteracion) {
		iteracionHome.delete(iteracion);
	}
	
	private List<UserStorie> getUserStories(Iteracion iteracion){
		return iteracionHome.findUserStories(iteracion.getId());
	}
	
	private List<UserStorie> getUserStoriesPorHacer(Iteracion iteracion){
		return iteracionHome.findUserStoriesPorHacer(iteracion.getId());
	}
	
	private List<UserStorie> getUserStoriesHaciendo(Iteracion iteracion){
		return iteracionHome.findUserStoriesHaciendo(iteracion.getId());
	}
	
	private List<UserStorie> getUserStoriesHecho(Iteracion iteracion){
		return iteracionHome.findUserStoriesHecho(iteracion.getId());
	}
	
	public String getSumaEstimacion(Iteracion iteracion){
		int i = 0;
		int sum = 0;
		List<UserStorie> list = this.getUserStories(iteracion);
		while(i < list.size()){
			sum += list.get(i).getEstimacion();
			i++;
		}
		return Integer.toString(sum);
	}

	public String getSumaEstimacionPorHacer(Iteracion iteracion) {
		return this.calcularEstimacion(this.getUserStoriesPorHacer(iteracion));
	}

	public String getSumaEstimacionHaciendo(Iteracion iteracion) {
		return this.calcularEstimacion(this.getUserStoriesHaciendo(iteracion));
	}

	public String getSumaEstimacionHecho(Iteracion iteracion) {
		return this.calcularEstimacion(this.getUserStoriesHecho(iteracion));
	}
	
	private String calcularEstimacion(List<UserStorie> userStories){
		int i = 0;
		int sum = 0;
		List<UserStorie> list = userStories;
		while(i < list.size()){
			sum += list.get(i).getEstimacion();
			i++;
		}
		return Integer.toString(sum);
	}
	
	public String getEstadoIteracion(Iteracion iteracion){
		return iteracion.getAbierto() ? "Abierto" : "Cerrado";
	}
	
	
}
