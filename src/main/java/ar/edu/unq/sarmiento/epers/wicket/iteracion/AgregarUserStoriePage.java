package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.wicket.proyecto.HomeProyecto;

public class AgregarUserStoriePage extends WebPage{
	
	@SpringBean(name="agregarUserStorieController")
	private AgregarUserStorieController controller;
	
	public AgregarUserStoriePage(int proyectoId, int iteracionId) {
		super();
		controller.setProyectoId(proyectoId);
		controller.setIteracionId(iteracionId);
		
		this.add(new Label("nombreIteracion", new PropertyModel<>(controller, "nombreIteracion")));
		ListView<UserStorie> listUserStorie = new ListView<UserStorie>("userStories", new PropertyModel<>(controller, "userStorieSinAsignar")) {

			@Override
			protected void populateItem(ListItem<UserStorie> item) {
				UserStorie userStorie = item.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombre", userStorieModel.bind("nombre"));
				
				Form<AgregarUserStorieController> formUserStories = new Form<AgregarUserStorieController>("formUserStories"){
					
				};
				formUserStories.add(nombreUserStorie);
				
				Link<String> agregarUserStorieBtn = new Link<String>("agregarBtn"){

					@Override
					public void onClick() {
						controller.agregarIteracio(userStorie);
						this.setResponsePage(new AgregarUserStoriePage(proyectoId, iteracionId));
						
					}
		        };
				formUserStories.add(agregarUserStorieBtn);
				item.add(formUserStories);
			}
		};
		this.add(listUserStorie);
		ListView<UserStorie> listUserStorieAll = new ListView<UserStorie>("userStoriesAll", new PropertyModel<>(controller, "userStorieAsignadas")) {

			@Override
			protected void populateItem(ListItem<UserStorie> item) {
				UserStorie userStorie = item.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserSorie = new Label("nombreUserStorie", userStorieModel.bind("nombre"));
				item.add(new Link<String>("quitarUserStorie"){
					@Override
					public void onClick() {
						controller.quitarUserStorie(userStorie);
						this.setResponsePage(new AgregarUserStoriePage(proyectoId, iteracionId));
					}
				});
				item.add(nombreUserSorie);
			}
		};
		this.add(listUserStorieAll);
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new DetalleIteracionPage(proyectoId, iteracionId));
			}
			
		});
	}
	
}
