package ar.edu.unq.sarmiento.epers.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

@Entity
public class UsuarioProyecto extends Persistible{
	@OneToOne
	private Proyecto proyecto;
	@OneToOne
	private Usuario usuario;
	@Enumerated(EnumType.STRING)
	private Rol rol;
	
	public UsuarioProyecto() {
		
	}
	
	public UsuarioProyecto(Proyecto proyecto, Usuario usuario, Rol rol){
		this.proyecto = proyecto;
		this.usuario = usuario;
		this.rol = rol;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Proyecto getProyecto() {
		return proyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
