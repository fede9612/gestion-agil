package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class DetalleUserStorieController implements Serializable{

	@Autowired
	private Home<UserStorie> userStorieHome;
	private UserStorie userStorie;

	public UserStorie getUserStorie() {
		return userStorie;
	}

	public void setUserStorie(UserStorie userStorie) {
		this.userStorie = userStorie;
	}
	
	public String getNombre(){
		return this.getUserStorie().getNombre();
	}

	public void attach(UserStorie userStorie) {
		userStorieHome.attach(userStorie);
		this.setUserStorie(userStorie);
	}
	
	public String getNombreIteracion(){
		if(this.noTieneIteracion()){
			return "No tiene";
		}else{
			return this.userStorie.getIteracion().getNombre();			
		}
	}

	private boolean noTieneIteracion() {
		return this.getUserStorie().getIteracion() == null;
	}
	
}
