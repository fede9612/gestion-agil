package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.home.ProyectoHome;
import ar.edu.unq.sarmiento.epers.model.Estado;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class CrearUserStorieController implements Serializable{

	@Autowired
	private Home<UserStorie> userStorieHome;
	@Autowired
	private ProyectoHome proyectoHome;
	
	private int idProyecto;
	private String nombre;
	private String descripcion;
	private Iteracion iteracion;
	private String rol;
	private String para;
	
	
	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public void persistir() {
		UserStorie userStorie = new UserStorie();
		userStorie.setNombre(this.getNombre());
		userStorie.setDescripcion(this.crearDescripcion());
		userStorie.setEstado(Estado.PorHacer);
		this.setearIteracion(userStorie);
		Proyecto proyecto = userStorieHome.findById(this.getIdProyecto());
		proyecto.getBacklog().addUserStorie(userStorie);
		userStorieHome.saveOrUpdate(userStorie);
	}

	private String crearDescripcion() {
		return "Como " + this.getRol() + " quiero " + this.getDescripcion() + " para " + this.getPara();
	}

	private void setearIteracion(UserStorie userStorie) {
		if(!(this.getIteracion() == null)){
			userStorie.setIteracion(this.getIteracion());
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Iteracion getIteracion() {
		return iteracion;
	}

	public void setIteracion(Iteracion iteracion) {
		this.iteracion = iteracion;
	}
	
	public List<Iteracion> getIteraciones(){
		return proyectoHome.findIteraciones(getIdProyecto())
						   .stream()
						   .filter(i -> i.getAbierto()).collect(Collectors.toList());
	}
	
	public List<String> getRoles(){
		return proyectoHome.find(this.getIdProyecto()).getRoles();
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getPara() {
		return para;
	}

	public void setPara(String para) {
		this.para = para;
	}
	
}
