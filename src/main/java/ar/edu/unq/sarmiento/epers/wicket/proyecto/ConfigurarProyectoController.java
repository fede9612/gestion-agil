package ar.edu.unq.sarmiento.epers.wicket.proyecto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.home.UsuarioHome;
import ar.edu.unq.sarmiento.epers.home.UsuarioProyectoHome;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.Rol;
import ar.edu.unq.sarmiento.epers.model.Usuario;
import ar.edu.unq.sarmiento.epers.model.UsuarioProyecto;

@Service
@Scope(value= ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class ConfigurarProyectoController implements Serializable{
	private static final long serialVersionUID = 1L;

	@Autowired
	private Home<Proyecto> proyectoHome;
	@Autowired
	private UsuarioHome usuarioHome;
	@Autowired
	private UsuarioProyectoHome usuarioProyectoHome;
	private int idProyecto;
	private Proyecto proyecto;
	private Usuario usuario;
	private Rol rol;
	private int idUsuario;
	private String rolUserStorie;
	
	public ConfigurarProyectoController() {
	}

	public int getIdProyecto() {
		return idProyecto;
	}
	
	public Proyecto buscarProyecto() {
		return proyectoHome.findById(this.getIdProyecto());
	}
	
	
	public void eliminar() {
		proyecto = this.buscarProyecto();
		proyectoHome.delete(proyecto);
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}
	
	public List<Usuario> getUsuarios(){
		List<Usuario> listUsuarioEnProyecto = new ArrayList<>(this.getUsuariosParaUnProyecto(this.getIdProyecto()));
		List<Usuario> listAllUsuarios = new ArrayList<>(usuarioHome.findUsuarios());
		listAllUsuarios.removeAll(listUsuarioEnProyecto);
		return listAllUsuarios;
	}
	
	public List<Usuario> getUsuariosParaUnProyecto(int proyectoId){
		List<UsuarioProyecto> list = new ArrayList<>(usuarioProyectoHome.findUsuariosProyecto(proyectoId));
		List<Usuario> listUsuario = new ArrayList<>();
		for (int i = 0; i < list.size(); ++i) {
			listUsuario.add(usuarioHome.findByIdUsuario(list.get(i).getUsuario().getId()));
		}
		return listUsuario;
	}

	public List<Rol> getRoles(){
		List<Rol> roles = new ArrayList<>();
		roles.add(Rol.administrador);
		roles.add(Rol.desarrollador);
		roles.add(Rol.dueño);
		return roles;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public void agregarUsuario(int idProyecto) {
		UsuarioProyecto usuarioProyecto = new UsuarioProyecto(this.getProyecto(), this.getUsuario(), this.getRol());
		usuarioProyectoHome.saveOrUpdate(usuarioProyecto);
	}

	public List<UsuarioProyecto> getUsuariosEnProyecto(){
		return usuarioProyectoHome.findUsuariosProyecto(this.getIdProyecto());
	}
	
	public String getNombreUsuario(){
		return usuarioHome.find(this.getIdUsuario()).getNombre();
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Proyecto getProyecto() {
		return proyectoHome.findById(this.getIdProyecto());
	}

	public void quitarUsuario(int idUsuarioProyecto) {
		usuarioProyectoHome.delete(usuarioProyectoHome.find(idUsuarioProyecto));
	}

	public void agregarRol() {
		Proyecto proyecto = this.getProyecto();
		proyecto.addRol(this.getRolUserStorie());
		this.proyectoHome.saveOrUpdate(proyecto);
	}

	public String getRolUserStorie() {
		return rolUserStorie;
	}

	public void setRolUserStorie(String rolUserStorie) {
		this.rolUserStorie = rolUserStorie;
	}
}
