package ar.edu.unq.sarmiento.epers.home;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.Proyecto;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

public interface Home<T> extends Serializable {

	public Session getSession();
	public T findByName(String name);
	public T find(Integer id);
	public void saveOrUpdate(T object);
	public void delete(T object);
	public void attach(T result);
	public List<Proyecto> findAll();
	public Proyecto findById(int id);
	public List<Iteracion> findIteraciones(int proyectoId);
	public List<UserStorie> findUserStoriesPorHacer(Integer id);
}
