package ar.edu.unq.sarmiento.epers.model;

public enum Estado {
	PorHacer("Por hacer"), Haciendo("Haciendo"), Hecho("Hecho");
	
	private String estado;
	
	private Estado(String estado) {
		this.estado = estado;
	}

	public String getString() {
		return estado;
	}
}
