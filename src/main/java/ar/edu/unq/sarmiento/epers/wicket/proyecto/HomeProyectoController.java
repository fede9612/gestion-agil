package ar.edu.unq.sarmiento.epers.wicket.proyecto;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.Home;
import ar.edu.unq.sarmiento.epers.model.Proyecto;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class HomeProyectoController implements Serializable{
	
	@Autowired
	private Home<Proyecto> proyectoHome;
	
	
	public List<Proyecto> getProyectos(){
		return  proyectoHome.findAll();
	}
}
