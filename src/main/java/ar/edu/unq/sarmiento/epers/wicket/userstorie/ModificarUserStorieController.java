package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.edu.unq.sarmiento.epers.home.ProyectoHome;
import ar.edu.unq.sarmiento.epers.home.UserStorieHome;
import ar.edu.unq.sarmiento.epers.model.Iteracion;
import ar.edu.unq.sarmiento.epers.model.UserStorie;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional
public class ModificarUserStorieController implements Serializable {

	@Autowired
	private UserStorieHome userStorieHome;
	@Autowired
	private ProyectoHome proyectoHome;
	private int userStorieId;
	private UserStorie userStorie;
	private String nombre;
	private String descripcionActual;
	private String descripcion;
	private Iteracion iteracion;
	private int proyectoId;
	private String para;
	private String rol;

	public void cargarAtributos() {
		nombre = this.getUserStorie().getNombre();
		descripcionActual = this.getUserStorie().getDescripcion();
		iteracion = this.getUserStorie().getIteracion();
	}

	public UserStorie getUserStorie() {
		return userStorieHome.find(userStorieId);
	}

	public void setUserStorie(UserStorie userStorie) {
		this.userStorie = this.getUserStorie();
	}

	public void attach(UserStorie userStorie) {
		userStorieHome.attach(userStorie);
		this.setUserStorie(userStorie);
	}

	public void modificar() {
		this.setUserStorie(getUserStorie());
		userStorie.setNombre(nombre);
		userStorie.setIteracion(this.getIteracion());
		userStorie.setDescripcion(this.crearDescripcion());
		userStorieHome.saveOrUpdate(userStorie);
	}

	private String crearDescripcion() {
		return "Como " + this.getRol() + " quiero " + this.getDescripcion() + " para " + this.getPara();
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Iteracion getIteracion() {
		return iteracion;
	}

	public void setIteracion(Iteracion iteracion) {
		this.iteracion = iteracion;
	}

	public List<Iteracion> getIteraciones() {
		return proyectoHome.findIteraciones(this.getProyectoId())
				   .stream()
				   .filter(i -> i.getAbierto()).collect(Collectors.toList());
	}

	public int getProyectoId() {
		return proyectoId;
	}

	public void setProyectoId(int proyectoId) {
		this.proyectoId = proyectoId;
	}

	public int getUserStorieId() {
		return userStorieId;
	}

	public void setUserStorieId(int userStorieId) {
		this.userStorieId = userStorieId;
	}

	public void quitarIteracion() {
		this.setIteracion(null);
	}

	public String getPara() {
		return para;
	}

	public void setPara(String para) {
		this.para = para;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public List<String> getRoles() {
		return proyectoHome.find(this.getProyectoId()).getRoles();
	}

	public String getDescripcionActual() {
		return descripcionActual;
	}

	public void setDescripcionActual(String descripcionActual) {
		this.descripcionActual = descripcionActual;
	}

}
