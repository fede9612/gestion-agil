package ar.edu.unq.sarmiento.epers.model;

import javax.persistence.Entity;

@Entity
public class Usuario extends Persistible{
	
	private String nombre;
	
	public Usuario() {}
	
	public Usuario(String nombre){
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
