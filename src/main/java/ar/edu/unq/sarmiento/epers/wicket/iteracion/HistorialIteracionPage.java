package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;

public class HistorialIteracionPage extends WebPage{

	@SpringBean(name="historialIteracionController")
	private HistorialIteracionController controller;
	
	public HistorialIteracionPage(int iteracionId,int proyectoId) {
		controller.setIteracionId(iteracionId);
		
		this.add(new Label("estimacionTotal", new PropertyModel<>(controller, "estimacionTotal")));
		this.add(new Label("estimacionTerminada", new PropertyModel<>(controller, "estimacionTerminada")));
		this.add(new Label("estimacionNoTerminada", new PropertyModel<>(controller, "estimacionNoTerminada")));
		
		ListView<UserStorie> listUserStorieHecho = new ListView<UserStorie>("userStoriesHecho", new PropertyModel<>(controller, "userStoriesHecho")) {

			@Override
			protected void populateItem(ListItem<UserStorie> item) {
				UserStorie userStorie = item.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombreHecho", userStorieModel.bind("nombre"));
				Label estimacionUserStorie = new Label("estimacionHecho", userStorieModel.bind("estimacion"));
				item.add(nombreUserStorie);
				item.add(estimacionUserStorie);
		        };
		  
		};
		
		ListView<UserStorie> listUserStorieHaciendo = new ListView<UserStorie>("userStoriesHaciendo", new PropertyModel<>(controller, "userStoriesHaciendo")) {

			@Override
			protected void populateItem(ListItem<UserStorie> item) {
				UserStorie userStorie = item.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombreHaciendo", userStorieModel.bind("nombre"));
				Label estimacionUserStorie = new Label("estimacionHaciendo", userStorieModel.bind("estimacion"));
				item.add(nombreUserStorie);
				item.add(estimacionUserStorie);
		        };
		  
		};
		
		ListView<UserStorie> listUserStoriePorHacer = new ListView<UserStorie>("userStoriesPorHacer", new PropertyModel<>(controller, "userStoriesPorHacer")) {

			@Override
			protected void populateItem(ListItem<UserStorie> item) {
				UserStorie userStorie = item.getModelObject();
				CompoundPropertyModel<UserStorie> userStorieModel = new CompoundPropertyModel<>(userStorie);
				Label nombreUserStorie = new Label("nombrePorHacer", userStorieModel.bind("nombre"));
				Label estimacionUserStorie = new Label("estimacionPorHacer", userStorieModel.bind("estimacion"));
				item.add(nombreUserStorie);
				item.add(estimacionUserStorie);
		        };
		  
		};
		
		
		this.add(listUserStorieHecho);
		this.add(listUserStorieHaciendo);
		this.add(listUserStoriePorHacer);
		this.add(new Link<String>("volver") {
			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(proyectoId));
			}
		});
	}
	
}
