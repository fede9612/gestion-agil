package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.BotonConfirmar;
import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;

public class EliminarIteracionPage extends WebPage{

	@SpringBean(name="modificarIteracionController")
	private ModificarIteracionController controller;
	
	public EliminarIteracionPage(int interacionId, int proyectoId) {
		super();
		controller.setIteracionId(interacionId);
		controller.setProyectoId(proyectoId);
		
		Form<ModificarIteracionController> formElimnarIteracion = new Form<ModificarIteracionController>("formEliminarIteracion"){
			
			@Override
			protected void onSubmit() {
				super.onSubmit();
				controller.eliminar();
				this.setResponsePage(new BacklogPage(proyectoId));
			}
		
		};
		
		formElimnarIteracion.add(new BotonConfirmar("confirmar", "Se eliminará por completo la Iteración"));
		this.add(formElimnarIteracion);
	}
	
}
