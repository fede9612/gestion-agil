package ar.edu.unq.sarmiento.epers.wicket.userstorie;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.model.UserStorie;
import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;

public class DetalleUserStoriePage extends WebPage{

	@SpringBean(name="detalleUserStorieController")
	private DetalleUserStorieController controller;
	
	public DetalleUserStoriePage(UserStorie userStorie, int idProyecto) {
		super();
		controller.attach(userStorie);
		
		this.add(new Label("nombreUserStorie", new PropertyModel<>(controller, "userStorie.nombre")));
		this.add(new Label("descripcionUserStorie", new PropertyModel<>(controller, "userStorie.descripcion")));
		this.add(new Label("iteracionUserStorie", new PropertyModel<>(controller, "nombreIteracion")));
		this.add(new Link<String>("volverBtn"){

			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(idProyecto));
			}
			
		});
	}
	
}
