package ar.edu.unq.sarmiento.epers.wicket.iteracion;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import ar.edu.unq.sarmiento.epers.wicket.backlog.BacklogPage;
import ar.edu.unq.sarmiento.epers.wicket.userstorie.ModificarUserStoriePage;

public class ModificarIteracionPage extends WebPage{

	@SpringBean(name="modificarIteracionController")
	private ModificarIteracionController controller;
	
	public ModificarIteracionPage(int iteracionId, int proyectoId) {
		super();
		controller.setIteracionId(iteracionId);
		controller.cargarDatos();
		
		Form<ModificarIteracionController> formModificarUserStorie = new Form<ModificarIteracionController>("formModificarIteracion"){
			
			@Override
			protected void onSubmit() {
				super.onSubmit();
				controller.modificar();
				this.setResponsePage(new ModificarIteracionPage(iteracionId,proyectoId));
			}
		
			
		};
		
		formModificarUserStorie.add(new TextField<>("nombreIteracion", new PropertyModel<>(controller, "nombre")));
		this.add(formModificarUserStorie);
		this.add(new Link<String>("eliminarIteracionBtn"){

			@Override
			public void onClick() {
				this.setResponsePage(new EliminarIteracionPage(iteracionId, proyectoId));
			}
			
			
		});
		this.add(new Link<String>("volver"){

			@Override
			public void onClick() {
				this.setResponsePage(new BacklogPage(proyectoId));
			}
			
			
		});
		
	}
	
	
}
